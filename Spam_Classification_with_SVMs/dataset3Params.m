function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

maxi = -1;
values = [0.01, 0.03, 0.1, 0.3, 1, 3, 10, 30];
for i=1:length(values)
	for j=1:length(values)
		model= svmTrain(X, y, values(i), @(x1, x2) gaussianKernel(x1, x2, values(j)));
		predictions = svmPredict(model, Xval);
		mu = mean(double(predictions ~= yval));
		fprintf("%f\n",mu);
		if (mu>maxi)
			maxi = mu;
			C = values(i);
			fprintf("%f\n",C);
			sigma = values(j);
			fprintf("%f\n",sigma);
		endif
	end

end

end
