function [J, grad] = cofiCostFunc(params, Y, R, num_users, num_movies, ...
                                  num_features, lambda)
%COFICOSTFUNC Collaborative filtering cost function
%   [J, grad] = COFICOSTFUNC(params, Y, R, num_users, num_movies, ...
%   num_features, lambda) returns the cost and gradient for the
%   collaborative filtering problem.
%

X = reshape(params(1:num_movies*num_features), num_movies, num_features);
Theta = reshape(params(num_movies*num_features+1:end), ...
                num_users, num_features);

h = X*Theta';
J = (sum(sum(((h-Y).*R).^2))+(lambda*(Theta(:)'*Theta(:)+X(:)'*X(:))))/2;
X_grad = (((h-Y).*R)*Theta)+(lambda*X);
Theta_grad = (((h-Y).*R)'*X)+(lambda*Theta);
grad = [X_grad(:); Theta_grad(:)];

end
