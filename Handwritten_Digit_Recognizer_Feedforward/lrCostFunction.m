function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 


m = length(y); 
h=sigmoid(X*theta);
J = sum(-y'*log(h)-(1-y)'*log(1-h))/m + (lambda*sum(theta(2:end,:)'*theta(2:end,:)))/(2*m);
grad_reg = lambda * theta;
grad_reg ( 1 ) = 0;
grad= ((X'*(h-y))+grad_reg)/m;

end
