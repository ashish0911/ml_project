function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

K = size(centroids, 1);

idx = ones(size(X,1), 1);

for i=1:size(X,1)
	dis_m = sum((X(i,:)-centroids(1, :)).^2)^(1/2);
	for j=2:size(centroids, 1)
		dis = sum((X(i,:)-centroids(j, :)).^2)^(1/2);
		if (dis_m >= dis)
			dis_m = dis;
			idx(i) = j;
		endif
	end
end

end

