function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));
m = size(X, 1);
a1 = [ones(m,1),X];
z2 = [ones(m,1), a1*Theta1'];
a2 = sigmoid(z2);
z3 = a2*Theta2';
h = sigmoid(z3);
y_l = (ones(m,1)*(1:num_labels)==y); 
J = (-sum(sum(y_l.*log(h)+(1-y_l).*log(1-h)))/m) + (lambda/(2*m))*(sum(sum(Theta1(:,2:end).^2)) + sum(sum(Theta2(:,2:end).^2)));
delta3 = h-y_l;
delta2 = (delta3*Theta2).*sigmoidGradient(z2);
tri2 = delta3'*a2;
tri1 = delta2(:,2:end)'*a1;
reg2 = (lambda/m)*[zeros(size(Theta2, 1),1), Theta2(:,2:end)];
reg1 = (lambda/m)*[zeros(size(Theta1, 1),1), Theta1(:,2:end)];
Theta1_grad = (tri1/m)+ reg1;
Theta2_grad = (tri2/m)+ reg2;

grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
